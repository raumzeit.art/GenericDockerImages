#!/usr/bin/bash

. ./dockerBuild.BuildSystem.sh

export ZLIB_VERSION_NR=$3

docker build --target zlibBuildSystem -f Dockerfile.zlib --build-arg ZLIB_DOWNLOAD_URL=$1 --build-arg ZLIB_VERSION=$2 -t raumzeit/zlib-buildsystem:latest -t raumzeit/zlib-buildsystem:$3 .
docker build -f Dockerfile.zlib --build-arg ZLIB_DOWNLOAD_URL=$1 --build-arg ZLIB_VERSION=$2 -t raumzeit/zlib:latest -t raumzeit/zlib:$3 .