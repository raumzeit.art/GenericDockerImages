#!/usr/bin/bash

export ZLIB_VERSION_NR=1.2.12
export OPENSSL_VERSION_NR=$3

docker build --target opensslBuildSystem -f Dockerfile.Openssl --build-arg OPENSSL_DOWNLOAD_URL=$1 --build-arg OPENSSL_VERSION=$2 -t raumzeit/openssl-buildsystem:latest -t raumzeit/openssl-buildsystem:$3 .
docker build -f Dockerfile.Openssl --build-arg OPENSSL_DOWNLOAD_URL=$1 --build-arg OPENSSL_VERSION=$2 -t raumzeit/openssl:latest -t raumzeit/openssl:$3 .